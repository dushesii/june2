docker build -t nginx1 .

docker run --name nginx1 -d -p 8081:80/tcp --rm \
 --mount type=bind,source="$(pwd)"/conf/nginx.conf,target=/usr/local/nginx/conf/nginx.conf \
 nginx1

